import * as React from 'react';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
//import { useGlobalContext } from '../App';
import Login from './Login';

const columns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 100 },
  { field: 'image_name', headerName: 'Image name', width: 200 },
  { field: 'size', headerName: 'Size', type:'number', width: 120 },
  { field: 'recognition_result', headerName: 'Recognition result', type:'number', width: 200 },
  {
    field: 'link',
    headerName: 'Download link',
    width: 200,
  },
];

const rows = [
  {id:1, image_name : 'Okonjima_Lioness.jpg', size: 10, recognition_result: 21, link: 'https://upload.wikimedia.org/wikipedia/commons/c/c6/Okonjima_Lioness.jpg' },
  {id:2, image_name : 'Staffordshire_Bull_Terrier_600.jpg', size: 5, recognition_result: 7, link: 'https://upload.wikimedia.org/wikipedia/commons/1/1a/Staffordshire_Bull_Terrier_600.jpg' },
  {id:3, image_name : 'Cow_%28Fleckvieh_breed%29_Oeschinensee_Slaunger_2009-07-07.jpg', size: 2, recognition_result: 15, link: 'https://upload.wikimedia.org/wikipedia/commons/8/8c/Cow_%28Fleckvieh_breed%29_Oeschinensee_Slaunger_2009-07-07.jpg' },
  {id:4, image_name : 'Sphinx2_July_2006.jpg', size: 5, recognition_result: 3, link: 'https://upload.wikimedia.org/wikipedia/commons/e/e8/Sphinx2_July_2006.jpg' },
  {id:5, image_name : 'Golden_retriever-035.JPG', size: 3, recognition_result: 21, link: 'https://upload.wikimedia.org/wikipedia/commons/d/d6/Golden_retriever-035.JPG' },
];


const DataTable = () => {
  const  isLogged  = true;//useGlobalContext()

  if (isLogged===true) {
  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        checkboxSelection
      />
    </div>
  );
  }
  return //<Login />
  
}
export default DataTable
