import React from 'react';
import { styled } from '@mui/material/styles';
import Button from '@material-ui/core/Button';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { Grid } from '@mui/material';

const Input = styled('input')({
    display: 'none',
});

const AppUpload = () => {

    return (
        <Container sx={{ marginTop: 10, marginLeft: 50, paddingBottom: 7, paddingRight: 10, margin:'auto', bgcolor: 'lightgray' }} maxWidth='md'>
            <Stack spacing={5} alignItems='center'>
                <Grid container sx={{ marginTop: 5, marginRight: 13 }} alignItems='center' justifyContent='flex-end'>
                    <Grid item>
                        <label htmlFor="contained-button-file">
                            <Input accept="image/*" id="contained-button-file" multiple type="file" />
                            <Button variant="contained" component="span">
                                Upload
                            </Button>
                        </label>
                    </Grid>
                </Grid>
                <Box sx={{ marginBottom: 5 }} bgcolor='gray' height={450} width={750} display='flex' alignItems='center' justifyContent='center'>
                    Image
                </Box>
            </Stack>
        </Container>
    );
}

export default AppUpload;