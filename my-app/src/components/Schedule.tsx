import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import React, { useEffect, useState } from "react";
import TextField from "@mui/material/TextField";
import StaticDateRangePicker from "@mui/lab/StaticDateRangePicker";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import Box from "@mui/material/Box";
import { DateRange } from "@mui/lab/DateRangePicker";
import Button from "@mui/material/Button";
import axios from "axios";

export default function Schedule() {
  const [appState, setAppState] = useState({
    rows: [],
  });
  
  
  

  const [value, setValue] = React.useState<DateRange<Date>>([null, null]);

  function createDate(){
      var start=value[0];
      var end=value[1];
      if(value[0]!=null){
      console.log(value[0].toString());
      }
      if(value[1]!=null){
      console.log(value[1].toString());
      }
    if(start!=null && end!=null){
    fetch("https://us-central1-fsd-project-334314.cloudfunctions.net/createSchedule", {
        "method": "POST",
        "headers": {
          "user-agent": "vscode-restclient",
          "content-type": "application/json"
        },
        "body":  JSON.stringify({
          "start_date": start.toString(),
          "end_date": end.toString()
        })
      })
      .then(response => {
        getSchedule()
      })
      .catch(err => {
        console.error(err);
      });
    }
  }

 function getSchedule(){
    const apiUrl = `https://us-central1-fsd-project-334314.cloudfunctions.net/getSchedule`;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((repos) => {
        setAppState({ rows: repos });
      });
  }

  useEffect(() => {
    getSchedule()
  }, [setAppState]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={6}>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell align="right">Start</TableCell>
                <TableCell align="right">End</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {appState.rows.map((row: any) => (
                <TableRow
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.start_date}
                  </TableCell>
                  <TableCell align="right">{row.end_date}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <Grid item xs={6}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <StaticDateRangePicker
            displayStaticWrapperAs="desktop"
            value={value}
            onChange={(newValue: any) => {
              setValue(newValue);
              console.log(newValue[0].toString());
              console.log(newValue[1]);
              if (newValue[1] != null) {
                console.log("new");
              }
            }}
            renderInput={(startProps: any, endProps: any) => (
              <React.Fragment>
                <TextField {...startProps} />
                <Box sx={{ mx: 2 }}> to </Box>
                <TextField {...endProps} />
              </React.Fragment>
            )}
          />
        </LocalizationProvider>
        <Button variant="contained" onClick={createDate}>Submit</Button>
      </Grid>
    </Grid>
  );
}
